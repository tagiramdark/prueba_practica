
export interface APIResponse<T>{
    results: T;
}
export interface DataResponse{
    personas:APIResponse<persona[]>;
    searchPersona:APIResponse<persona[]>;
    persona:APIResponse<persona>;
}
export interface persona {
    id:number;
    nombre:string;
    apellidoPaterno:string;
    apellidoMaterno:string;
    direccion:string;
    telefono:string;
   
}