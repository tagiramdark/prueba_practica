import { Apollo, gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import { take, pluck, tap, catchError, map } from 'rxjs/operators';
import { DataResponse, persona } from '../interfaces/data.interface';
import { BehaviorSubject, of } from 'rxjs';

const QUERY = gql`
  query {
    personas {
      results {
        id
        nombre
        apellidoPaterno
        apellidoMaterno
        direccion
        telefono
      }
    }
  }
`;
@Injectable({
  providedIn: 'root',
})
export class DataService {
  private personaSubject = new BehaviorSubject<persona[]>([]);
  personas$ = this.personaSubject.asObservable();

  constructor(private apollo: Apollo) {
    this.getDataAPI();
  }

  createPersona(persona: persona): void {
    const mutation = gql`
      mutation createPersona($input: CreatePersonaInput) {
        createPersona(input: $input) {
          results {
            id
            nombre
            apellidoPaterno
            apellidoMaterno
            direccion
            telefono
          }
        }
      }
    `;
    this.apollo
      .mutate<any>({
        mutation,
        variables: {
          input: {
            nombre: persona.nombre,
            apellidoPaterno: persona.apellidoPaterno,
            apellidoMaterno: persona.apellidoMaterno,
            direccion: persona.direccion,
            telefono: persona.telefono.toString(),
          },
        },
      })
      .pipe(
        take(1),
        pluck('data'),
        tap((data) => {
          const { createPersona } = data;
          this.personaSubject.next(createPersona.results);
        })
      )
      .subscribe();
  }
  deletePersona(persona: persona): void {
    const mutation = gql`
      mutation deletePersona($input: PersonaID) {
        deletePersona(input: $input) {
          results {
            id
            nombre
            apellidoPaterno
            apellidoMaterno
            direccion
            telefono
          }
        }
      }
    `;
    console.log(persona);
    this.apollo
      .mutate<any>({
        mutation,
        variables: {
          input: {
            id: persona.id           
          },
        },
      })
      .pipe(
        take(1),
        pluck('data'),
        tap((data) => {
          const { deletePersona } = data;
          this.personaSubject.next(deletePersona.results);
        })
      )
      .subscribe();
  }
  updatePersona(persona: persona): void {
    const mutation = gql`
      mutation updatePersona($input: UpdatePersonaInput) {
        updatePersona(input: $input) {
          results {
            id
            nombre
            apellidoPaterno
            apellidoMaterno
            direccion
            telefono
          }
        }
      }
    `;
    console.log(persona);
    this.apollo
      .mutate<any>({
        mutation,
        variables: {
          input: {
            id: persona.id,
            nombre: persona.nombre,
            apellidoPaterno: persona.apellidoPaterno,
            apellidoMaterno: persona.apellidoMaterno,
            direccion: persona.direccion,
            telefono: persona.telefono.toString(),
          },
        },
      })
      .pipe(
        take(1),
        pluck('data'),
        tap((data) => {
          const { updatePersona } = data;
          this.personaSubject.next(updatePersona.results);
        })
      )
      .subscribe();
  }
  searchPersona(valuetosearch: string) {
    const QUERY_SEARCH = gql`
      query ($search: String!) {
        searchPersona(search: $search) {
          results {
            id
            nombre
            apellidoPaterno
            apellidoMaterno
            direccion
            telefono
          }
        }
      }
    `;

    this.apollo
      .watchQuery<DataResponse>({
        query: QUERY_SEARCH,
        variables: {
          search: valuetosearch,
        },
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { searchPersona } = data;
          this.personaSubject.next(searchPersona.results);
        }),
        catchError((error) => {
          console.log(error.message);
          this.personaSubject.next([]);
          return of(error);
        })
      )
      .subscribe();
  }
  private getDataAPI(): void {
    this.apollo
      .watchQuery<DataResponse>({
        query: QUERY,
      })
      .valueChanges.pipe(
        take(1),
        tap(({ data }) => {
          const { personas } = data;
          this.personaSubject.next(personas.results);
        })
      )
      .subscribe();
  }
}
