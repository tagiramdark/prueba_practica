import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import {
  debounceTime,
  distinctUntilChanged,  
  map,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { PersonComponent } from '../person/person.component';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataService } from '../../../services/data.service';
@Component({
  selector: 'app-persontable',
  templateUrl: './persontable.component.html',
  styleUrls: ['./persontable.component.scss'],
})
export class PersontableComponent implements OnInit, OnDestroy {
  personas$ = this.dataService.personas$;
  displayedColumns: string[] = [
    'nombre',
    'apellidoPaterno',
    'apellidoMaterno',
    'direccion',
    'telefono',
    'action',
  ];
  search = new FormControl('');
  dataSource: MatTableDataSource<any>;
  private destroy$ = new Subject<unknown>();

  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  @ViewChild('sort') sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog, private dataService: DataService) {
    this.onSearch();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }
  ngOnInit() {
    this.personas$.subscribe((personas) => {
      this.dataSource = new MatTableDataSource(personas);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openDialog(action: string, obj: any) {
    let data = JSON.parse(JSON.stringify(obj));
    data.action = action;
    const dialogRef = this.dialog.open(PersonComponent, {
      width: action !== 'Borrar' ? '40%' : '15%',
      data: data,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.event == 'Agregar') {
        this.addRowData(result.data);
      } else if (result.event == 'Editar') {
        this.updateRowData(result.data);
      } else if (result.event == 'Borrar') {
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(row_obj: any) {
    this.dataService.createPersona(row_obj);
  }
  updateRowData(row_obj: any) {
    this.dataService.updatePersona(row_obj);
  }
  deleteRowData(row_obj: any) {
    this.dataService.deletePersona(row_obj);
  }
  private onSearch(): void {
    this.search.valueChanges
      .pipe(
        map((search) => search?.toLowerCase().trim()),
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this.destroy$),
        tap((search) => this.dataService.searchPersona(search))
      )
      .subscribe();
  }
}
